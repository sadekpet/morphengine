
#ifndef MORPH_RESOURCE_STORAGE_HPP
#define MORPH_RESOURCE_STORAGE_HPP

#include <Core/Core.hpp>

namespace Morph {

class ResourceStorage
{
private:
    unord_map<string, string> m_resFoldersToPaths;
    string m_resSeparator;
public:
    ResourceStorage(string resSeparator = ":");
    ResourceStorage(const unord_map<string, string>& resFoldersToPaths, string resSeparator = ":");

    void InsertResFolder(const string& name, const string& path);
    opt<string> GetResFolderPath(const string& resFolder) const;
    opt<string> GetPath(const string& resFolderRelPath) const;
    opt<string> GetPath(const string& resFolder, const string& relPath) const;

    opt<string> ReadFileRes(const string& resFolderRelPath) const;
    static opt<string> ReadFile(const string& path);

};

}

#endif // MORPH_RESOURCE_STORAGE_HPP