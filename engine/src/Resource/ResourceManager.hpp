
#ifndef MORPH_RESOURCE_MANAGER_HPP
#define MORPH_RESOURCE_MANAGER_HPP

#include <Core/Core.hpp>

#include <Graphics/Texture.hpp>
#include <Data/Mesh.hpp>

namespace Morph {

class ResourceManager
{
public:
    static void SaveTexture2D_PNG(const Texture2D& texture, string filename);
    static opt<IndexedVerticesMesh3D<u32>> LoadMesh3D_OBJ(string filename);
    static IndexedVerticesMesh3D<u32> RecalcualteNormals(IndexedVerticesMesh3D<u32> mesh);
};

}

#endif // MORPH_RESOURCE_MANAGER_HPP