#ifndef MORPH_WINDOW_APP_HPP
#define MORPH_WINDOW_APP_HPP

#include <Window/Manager.hpp>
#include <Graphics/Context.hpp>
#include <Profile/Timer.hpp>

#include "App.hpp"
#include "WindowAppConfig.hpp"

namespace Morph {

class WindowApp : public App
{
private:
    ExecutionTypeVar m_executionType;
    unique<WindowManager> m_windowManager;
    MethodAttacher<WindowManagerError, WindowApp> m_windowManagerErrorAttacher;
    WindowID m_windowID;
    ref<Window> m_window;
    unique<GraphicsContext> m_context;
    bool m_shouldClose = false;
    TimerResult m_lastIterTime;
    TimerResult m_lastFrameTime;
    Void d_updateExecutionType;
public:
    WindowApp(const WindowAppConfig& config);
    void Run() override;
protected:
    virtual void RunIter(f64 lastIterTime, f64 lastFrameTime) = 0;

    inline void SetShouldClose() { m_shouldClose = true; }

    inline void SetExecutionType(const ExecutionTypeVar& executionType) {
        m_executionType = executionType; UpdateExecutionType();
    }
    inline const ExecutionTypeVar& GetExecutionType() const { return m_executionType; }

    inline       WindowManager& windowManager()       { return *m_windowManager; }
    inline const WindowManager& windowManager() const { return *m_windowManager; }
    inline       Window& window()       { return m_window; }
    inline const Window& window() const { return m_window; }
    inline       GraphicsContext& context()       { return *m_context; }
    inline const GraphicsContext& context() const { return *m_context; }
private:
    void UpdateExecutionType();
    void OnWindowManagerError(const WindowManagerError& error);
};

}

#endif // MORPH_WINDOW_APP_HPP