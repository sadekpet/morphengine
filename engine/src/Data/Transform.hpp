
#ifndef MORPH_TRANSFORM_HPP
#define MORPH_TRANSFORM_HPP

#include <Core/Core.hpp>

namespace Morph {

struct Transform
{
    vec3 pos = vec3(0);
    vec3 scale = vec3(1);
    float rotAngle = 0;
    vec3 rotAxis  = vec3(0, 0, 1);
    mat4 ToMat();
    mat4 ToInv();
};

}

#endif // MORPH_TRANSFORM_HPP