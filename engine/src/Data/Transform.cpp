#include "Transform.hpp"

namespace Morph {


mat4 Transform::ToMat()
{
    return glm::translate(pos) * glm::rotate(rotAngle, rotAxis) * glm::scale(scale);
}
mat4 Transform::ToInv()
{
    return glm::scale(vec3(1) / scale) * glm::rotate(-rotAngle, rotAxis) * glm::translate(-pos);
}

}